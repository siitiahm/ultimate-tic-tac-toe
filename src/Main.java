import java.util.*;
import java.io.*;
import java.math.*;
import sun.reflect.generics.reflectiveObjects.*;

import static java.lang.Integer.parseInt;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player {

    public static void main(String args[]) {
        Random random = new Random();
        Scanner in = new Scanner(System.in);
        int[][] board = new int[3][3];

        // game loop
        while (true) {
            String move = getNextMove(in, random, board);
            System.out.println(move);
        }
    }

    public static String getNextMove(Scanner in, Random random, int[][] board) {
        int opponentRow = in.nextInt();
        int opponentCol = in.nextInt();
        int validActionCount = in.nextInt();
        List<String> validActions = new ArrayList<>(validActionCount);
        for (int i = 0; i < validActionCount; i++) {
            int row = in.nextInt();
            int col = in.nextInt();
            validActions.add(String.format("%s %s", row, col));
        }
        //TODO: go through the list of actions,
        // create a new state from them,
        // if any is a winner, return it
        // otherwise, return a random one
        String move = validActions.get(random.nextInt(validActions.size()));
        return move;
    }

    public static int[][] createStateFromAction(int player, String action, int[][] board) {
        //throw new RuntimeException("not implemented");
        int[][] newBoard;
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                newBoard
            }
        }
        String [] changingToNumber = action.split(" ");
        int row = parseInt(changingToNumber[0]);
        int col = parseInt(changingToNumber[1]);

        if(board[row][col] != 0)

    }

    public static int getWinner(int[][] board) {
      //  throw new NotImplementedException();
        for(int i = 0; i < 3; i++) {

            if(board[0][i] == board[1][i] && board[0][i] == board[2][i]) {
                return board[0][i];
            }//if

            if(board[i][0] == board[i][1] && board[i][0] == board[i][2]) {
                return board[i][0];
            }

            if(board[0][0] != 0 && board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
                return board[0][0];
            }

            if(board[2][0] != 0 && board[2][0] == board[1][1] && board[2][0] == board[0][2]) {
                return board[2][0];
            }

        }//forS
        return 0;
    }
}